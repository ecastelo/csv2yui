

App.selectingFile= Ember.View.extend({
  change: function(evt) {
    this.get('controller').send('selectFile', evt);
  }
});