App.FileUp= Ember.TextField.extend({
  type: 'file',

  change: function(evt) {
    var input = evt.target;
    var csvFile = new CSV();

    if (input.files && input.files[0]) {
//      var that = this;
      csvFile.name = input.files[0];
      var reader = new FileReader();
      reader.onloadend = function(e) {
        var data = e.target.result;
        csvFile.load(data);
        var c = App.ApplicationController.create();
        c.set("totalRows", csvFile.lines.length);
      }
      reader.readAsText(input.files[0]);
    }
  },
});

