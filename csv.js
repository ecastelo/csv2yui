function CSV(){
  this.name = '';
  this.header = '';
  this.lines = [];
};

CSV.prototype = (function(){

  //prototype
  return{
    constructor:CSV,

    getContent: function() {
      return this.lines;
    },

    setHeader: function(nkeys) {
      this.header = nkeys.split(',')
    },

    reset: function(){
      this.header = '';
      this.lines = [];
    },

    addLine: function(pos, values) {
      var obj = {};
      var val = values.split(',');
      obj['recid'] = pos;
      for (var i = 0, l = this.header.length; i < l; i++) {
          obj[this.header[i]] = val[i];
      }
      this.lines.push(obj);
    },

    is_empty: function(){
      return this.lines.length == 0
    },

    is_valid: function (file){
      var file_name = file.name;
      var extension = file_name.substr(file_name.lastIndexOf('.') + 1).toLowerCase();
      if (file_name.length < 1 || extension != 'csv' )
        return false;

      return true;
    },

    load: function(csvContent) {
      this.reset();
      var byLines = csvContent.split(/[\r\n]+/g);
      this.setHeader(byLines[0]);

      var i = 1;
      while (i < byLines.length && byLines[i] != '') {
        this.addLine(i, byLines[i]);
        i++;
      }
    },

    getRange: function(start, n){
      var content = this.getContent();
      return content.slice(start, start + n);
    }
  };
})();


