/**
 * Created by exc on 5/5/14.
 */
YUI.add('listview', function(Y) {

  Y.Row = Y.Base.create('row', Y.Model, [], {

    initializer: function (attr) {
      if (attr) {
        for (var i = 0; i < attr.length; i++) {
          this.set(attr[i], '');
        }

        this.theAttrs = attr;
      }
    },

    add: function (obj) {
      var attrs = this.theAttrs;
      for (var i = 0; i < attrs.length; i++) {
        this.set(attrs[i], obj[attrs[i]]);
      }
    }
  },{
    ATTRS : {
      theAttrs : {
        value: []
      }
    }
  });

  Y.ListView = Y.Base.create('ListView', Y.View, [], {
    initializer: function(){
      this.set('table', new Y.DataTable());
    },

    render: function (source) {
      var content = source.getContent();

      var table = new Y.DataTable({
        recordType: new Y.Row(source.header).theAttrs,
        data: content,
        scrollable: 'y',
        sortable: true,
        filters : 'auto'
      });

      this.set('table',table);
      this.get('table').render('#div-yui-result');
    }

  },{
    ATTRS :{
      table : { value: []}
    }
  });

}, '1.0', {requires: ['view','datatable','node','listview']});



YUI().use('node', 'listview', function(Y){
  var ListView = new Y.ListView();

  Y.one('#readableFileInput').on('change', function (ev) {
    var files = ev._currentTarget.files;
    Y.one('#div-yui-result').empty();
    var csvFile = new CSV();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);
          ListView.render(csvFile);
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });
});
