YUI().use('node', 'node-scroll-info','datatable-mutable', function(Y){

  var csvFile = new CSV();
  var dt = new Y.DataTable();
  var row = 0,
    ROWS_PER_SET = 50,
    TOTAL_ROWS = 0;

  var container = Y.one('body');
  container.plug(Y.Plugin.ScrollInfo);

  container.scrollInfo.on('scrollToBottom', function (e) {
    // Load more content when the user scrolls to the bottom of the page.
    if (row < TOTAL_ROWS){
      if (row + ROWS_PER_SET > TOTAL_ROWS) {
        ROWS_PER_SET = TOTAL_ROWS - row;
      }

      var arrToAdd = csvFile.getRange(row, ROWS_PER_SET);
      dt.addRows(arrToAdd);
      dt.render('#div-yui-result');
      row += ROWS_PER_SET;
      renderSummary();
    }
  });

  function renderSummary(){
    var text = "Total Records: " + TOTAL_ROWS + " ->  Currently rendered: " + row
    Y.one('#renderedLines').setContent(text);
  }


  Y.one('#readableFileInput').on('change', function(){
    csvFile = new CSV();
    row = TOTAL_ROWS = 0,
    ROWS_PER_SET = 20;

    var files = document.getElementById('readableFileInput').files;
    Y.one('#div-yui-result').empty();
    Y.one('#renderedLines').empty();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);
          TOTAL_ROWS = csvFile.lines.length
          dt = new Y.DataTable({
            column: csvFile.header,
            data: csvFile.getRange(row, ROWS_PER_SET),
            scrollable: 'y',
            sortable: true,
            filters : 'auto'
          });
          row += ROWS_PER_SET;
          dt.render('#div-yui-result');
          renderSummary();
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });
});