var Y = YUI().use('node-core', 'node-scroll-info','datatable-mutable', function(Y){

  var csvFile = new CSV();
  var dt = new Y.DataTable();
  var content = Y.one('body');

  var row = 0,
    rows_per_set = 50,
    total_rows = 0;


  function updateCounter(){
    var text = "Total Records: " + total_rows + " , rendered: " + dt.get("data").size()
    Y.one('#renderedLines').setContent(text);
  };

  function getPosition(){
    var windowY = window.pageYOffset;
    var scrollY = window.scrollY;

    percent_in_page = scrollY * 100 / windowY;
    pos_in_table = Math.round(percent_in_page * total_rows /100);

    return pos_in_table;
  };

  function displayRows() {
    if (total_rows > rows_per_set) {
      row = getPosition()
      dt.get("data").reset()
      if (row == 0) {
        var data = csvFile.getRange(row, rows_per_set)
        dt.addRows(data)
        dt.render('#div-yui-content')
        addSpace('bottom',300)
      } else if (row = total_rows) {
        row = row - rows_per_set
        var data = csvFile.getRange(row, rows_per_set)
        addSpace('top',300)
        dt.addRows(data)
        dt.render('#div-yui-content')

      } else if (row < total_rows) {
        var data = csvFile.getRange(row, rows_per_set)
        dt.addRows(data)
        dt.render('#div-yui-content')
        addSpace('top')
        addSpace('bottom')
      }
      updateCounter()
    }
  };

  function addSpace(where){
    var container = Y.one('table')
    var tr = document.createElement('tr')
    tr.appendChild(document.createElement('td'))
    switch(where){
      case 'top':
        var space_element = document.createElement('top_space')
        space_element.appendChild(tr)
        var first = container.firstChild
        container.insertBefore(space_element,first)
        break;
      case 'bottom':
        var space_element = document.createElement('bottom_space')
        space_element.appendChild(tr)
        container.appendChild(space_element)
        break;
    }

    redimSpace(where,npx)
  }

  function redimSpace(where,vheight){
    switch(where){
      case 'top':
        var space_element = Y.one('top_space')
        break;
      case 'bottom':
        var space_element = Y.one('bottom_space')
        break;
    }

    space_element.one('tr').setStyle('height', vheight + 'px')
  }

  function initialize(){
    var windowY = window.pageYOffset;
    var scrollY = window.scrollY;
    total_rows = csvFile.lines.length;
    var container = Y.one('#div-yui-content')
    var data = csvFile.getRange(row, rows_per_set)
    dt = new Y.DataTable({
      column: csvFile.header,
      data: data
    })
    dt.render('#div-yui-content')
    addSpace('bottom')
    updateCounter()
  };

  window.scrollInfo = content.plug(Y.Plugin.ScrollInfo, {scrollMargin: 0}).scrollInfo;

  Y.one('win').on('windowresize', initialize);

  content.scrollInfo.on('scrollDown', function (e) {
    if(content.scrollInfo.isNodeOnscreen('bottom_space')){
      displayRows();
    }
  });

  content.scrollInfo.on('scrollUp', function (e) {
    if(content.scrollInfo.isNodeOnscreen('top_space')){
      displayRows();
    }
  });

  Y.one('#readableFileInput').on('change', function(){
    csvFile = new CSV();
    row = total_rows = 0,
    rows_per_set = 50;

    var files = document.getElementById('readableFileInput').files;
    Y.one('#div-yui-content').empty();
    Y.one('#renderedLines').empty();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);
          initialize()
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });


});