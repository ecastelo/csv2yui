
var Y = YUI().use('node-scroll-info','datatable-mutable', function(Y){
  var csvFile = new CSV();
  var pages = new Pages();
  var dt = new Y.DataTable();

  var content = Y.one('body');

  var row = 0,
      rows_per_set = 20,
      total_rows = 0;

  var initial_table_height = 0;

  function updateCounter(){
    var text = "Total Records: " + total_rows + "    Currently rendered: " + row
    Y.one('#renderedLines').setContent(text);
  };

  function renderSpacer(){
    var spacer = Y.one('#spacer')
    var hspacer = initial_table_height * (pages.pages.length - 1)
    spacer.setStyle('height', hspacer)
  }

  function refresh_datatable(ini,fin){
    if()
  }

//  function getNodes(){
//    var nodes_even_on = content.scrollInfo.getOnscreenNodes('.yui3-datatable-even');
//    var nodes_odd_on = content.scrollInfo.getOnscreenNodes('.yui3-datatable-odd');
//    var on_screen = nodes_even_on.concat(nodes_odd_on);
//
//    var nodes_even_off = content.scrollInfo.getOffscreenNodes('.yui3-datatable-even');
//    var nodes_odd_off = content.scrollInfo.getOffscreenNodes('.yui3-datatable-odd');
//    var off_screen = nodes_even_off.concat(nodes_odd_off);
//  }

  function loadPage(page){
    Y.log("Page Range: [" + pages.min(page,rows_per_set)+ "," + pages.max(page,rows_per_set)+ "]")
//    if (row < total_rows){
//      if (row + rows_per_set > total_rows) {
//        rows_per_set = total_rows - row;
//      }
//
//      var arrToAdd = csvFile.getRange(row, rows_per_set);
//      dt.addRows(arrToAdd);
//      dt.render('#div-yui-content');
//      row += rows_per_set;
//      updateCounter();
//      renderSpacer();
//    }

    if (pages.isFirst(page) && (!pages.isPageLoaded(page) || !pages.isPageLoaded(page + 1) )) {
      dt.data.reset()
      pages.repaginate()
      var arrToAdd = csvFile.getRange(pages.min(page, rows_per_set), rows_per_set * 2);
      dt.addRows(arrToAdd);
      pages.markAsLoaded(page + 1);
      display(page)
      Y.log("*** Reseteando: isFirst and  Page " +  page)
      //    load_on_top(page)
      //    load_on_bottom(page + 1)
    } else if (pages.isLast(page) && (!pages.isPageLoaded(page) || !pages.isPageLoaded(page - 1))) {
      dt.data.reset()
      pages.repaginate()
      var arrToAdd = csvFile.getRange(pages.min(page - 1, rows_per_set),rows_per_set * 2);
      dt.addRows(arrToAdd);
      pages.markAsLoaded(page - 1);
      display(page)
      Y.log("*** Reseteando: isLast and  Page " +  page)
      //    load_on_top(page -1)
      //    load_on_bottom(page)
    } else if (!pages.isFirst(page) && !pages.isLast(page) &&  (!pages.isPageLoaded(page) || !pages.isPageLoaded(page - 1) || !pages.isPageLoaded(page + 1))){
      //      load_on_top(page -1)
      //      load_on_middle(page)
      //      load_on_bottom(page + 1)
      dt.data.reset()
      pages.repaginate()
      var arrToAdd = csvFile.getRange(pages.min(page - 1, rows_per_set), rows_per_set * 3);
      dt.addRows(arrToAdd);
      pages.markAsLoaded(page + 1);
      pages.markAsLoaded(page - 1);
      display(page)
      Y.log("*** Reseteando: NOT FIRST and NOT LAST and  Page " +  page)
    }
  };

  function display(page){
//    dt.render('#div-yui-content');
    pages.markAsLoaded(page);
    updateCounter();
    renderSpacer();
  };

  function getPage(line, n){
    return Math.trunc(line/n)
  };

  function getPageOnScreen(){
    var node_on_screen = content.scrollInfo.getOnscreenNodes('.yui3-datatable-even')
    var page;

    if(node_on_screen._nodes.length > 0) {
      page = getPage(node_on_screen._nodes[node_on_screen._nodes.length - 1].rowIndex, rows_per_set) + 1
    }else{
      var scrollInfo = content.plug(Y.Plugin.ScrollInfo);
      var scrollY = window.scrollY //scroll position
      var tbody_info = Y.one('tbody').plug(Y.Plugin.ScrollInfo)._node;
      var scrollTop = tbody_info.offsetTop
      page = Math.trunc((scrollY - scrollTop) / initial_table_height) + 1
    }
    Y.log("******************** Pages " + pages.totalPages + " and this should be: "+ page)
    return page
  };

  window.scrollInfo = content.plug(Y.Plugin.ScrollInfo, {scrollMargin: 0}).scrollInfo;

  content.scrollInfo.on('scrollDown', function (e) {
    var currentPage = getPageOnScreen()
//    if (!pages.isPageLoaded(currentPage)){
      loadPage(currentPage)
//    }
  });

  content.scrollInfo.on('scrollUp', function (e) {
    var currentPage = getPageOnScreen()
    if (!pages.isPageLoaded(currentPage)){
      loadPage(currentPage)
    }

    if (!pages.isPageLoaded(currentPage - 1)) {
      loadPage(currentPage - 1)
    }
  });

  Y.one('#readableFileInput').on('change', function(){
    csvFile = new CSV();
    row = total_rows = 0,
    rows_per_set = 50;

    var files = document.getElementById('readableFileInput').files;
    Y.one('#div-yui-content').empty();
    Y.one('#renderedLines').empty();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var selectedFile = e.target.result;
          csvFile.load(selectedFile);
          total_rows = csvFile.lines.length
          pages.paginate(total_rows, rows_per_set);
          dt = new Y.DataTable({
            column: csvFile.header,
            data: csvFile.getRange(pages.min(1, rows_per_set), rows_per_set * 2)
          });
          refresh_datatable();
          dt.render('#div-yui-content');
          row += rows_per_set;
          pages.markAsLoaded(1);
          pages.markAsLoaded(2);
          initial_table_height = parseInt(Y.one('tbody').getStyle('height'))
          renderSpacer();
          updateCounter();
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });


});