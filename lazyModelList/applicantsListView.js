YUI.add('applicantsListView', function(Y) {

  Y.ApplicantView = Y.Base.create('applicantview', Y.View, [],{
    template: '',

    initializer: function(){
      this.template = this._generateTemplate();
    },

    _generateTemplate: function(){
      var model = this.get('model');
      var attrs = model.theAttrs;
      var lineTemplate = '<tr>';

      if (attrs) {
        for (var i = 0; i < attrs.length; i++) {
          lineTemplate += '<td>{' + attrs[i] + '}</td>';
        }
      }

      lineTemplate += '</tr>';

      return lineTemplate;

    },

    render: function(parentContainer){
      var model = this.get('model');
      var content = Y.Lang.sub(this.template, model.toJSON());
      var container = this.get('container');
      container.append(content);
      if(container.get('parent') !== parentContainer){
        parentContainer.append(container);
      }
      return this;
    },

    renderHeader: function(parentContainer){
      var model = this.get('model');
      var headerContent = '<tr>';
      var attrs = model.theAttrs;
      if(attrs){
        for (var i = 0; i < attrs.length; i++){
          headerContent +='<th>' + attrs[i] + '</th>';
        }
      }
      headerContent += '</tr>';

      var container = this.get('container').empty();
      container.append(headerContent);
      if(container.get('parent') !== parentContainer){
        parentContainer.append(container);
      }

      return this;
    }

  },{
    ATTRS: {
      container: { valueFn: function(){return Y.one('#yui-table-result');} }
    }
  });

  Y.ApplicantsListView = Y.Base.create('applicantsListView', Y.View, [], {
    template: '<table id="yui-table-result"></table>',

    initializer: function () {
      var container = this.get('container').empty();
      container.plug(Y.Plugin.ScrollInfo);
      this.set('container',container);

      var listView = this.get('listView');
      this.after('listViewChange', function (ev) {
        ev.prevVal && ev.prevVal.removeTarget(this);
        ev.prevVal && ev.newVal.addTarget(this);
      });

      listView && listView.addTarget(this);
    },

    render: function (source) {
      var container = this.get('container').empty();
      container.append(this.template);
      Y.one('body').append(container);

      this._loadData(source);
      return true;
    },

    _loadData: function(source){
      this.set('listView', this.get('listView').reset());
      var content = source.getContent();
      for(var i = 0; i < content.length; i++){
        this.get('listView').add(content[i]);
      }
    }

  }, {
    ATTRS: {
      listView: {value: new Y.LazyModelList()},
      container: {value: Y.Node.create('<div id="div-yui-result"></div>')}
    }
  });

}, '1.0', {requires: ['lazy-model-list', 'view', 'node-scroll-info']});


YUI().use('node', 'applicantsListView', function(Y){

  var applicantsListView = new Y.ApplicantsListView();

  Y.one('#readableFileInput').on('change', function(){
    var files = document.getElementById('readableFileInput').files;
    Y.one('#div-yui-result').empty();
    var csvFile = new CSV();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);
          applicantsListView.render(csvFile);
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });
});