YUI.add('applicant', function(Y) {

  Y.Applicant = Y.Base.create('applicant', Y.Model, [], {

    initializer: function (attr) {
      if (attr) {
        for (var i = 0; i < attr.length; i++) {
          this.set(attr[i], '');
        }

        this.theAttrs = attr;
      }
    },

    add: function (obj) {
      var attrs = this.theAttrs;
      for (var i = 0; i < attrs.length; i++) {
        this.set(attrs[i], obj[attrs[i]]);
      }
    }
  },{
    ATTRS : {
      theAttrs : {
        value: []
      }
    }
  });

  Y.ApplicantView = Y.Base.create('applicantview', Y.View, [],{
    template: '',

    initializer: function(){
      this.template = this._generateTemplate();
    },

    _generateTemplate: function(){
      var model = this.get('model');
      var attrs = model.theAttrs;
      var lineTemplate = '<tr>';

      if (attrs) {
        for (var i = 0; i < attrs.length; i++) {
          lineTemplate += '<td>{' + attrs[i] + '}</td>';
        }
      }

      lineTemplate += '</tr>';

      return lineTemplate;

    },

    render: function(parentContainer){
      var model = this.get('model');
      var content = Y.Lang.sub(this.template, model.toJSON());
      var container = this.get('container');
      container.append(content);
      if(container.get('parent') !== parentContainer){
        parentContainer.append(container);
      }
      return this;
    },

    renderHeader: function(parentContainer){
      var model = this.get('model');
      var headerContent = '<tr>';
      var attrs = model.theAttrs;
      if(attrs){
        for (var i = 0; i < attrs.length; i++){
          headerContent +='<th>' + attrs[i] + '</th>';
        }
      }
      headerContent += '</tr>';

      var container = this.get('container').empty();
      container.append(headerContent);
      if(container.get('parent') !== parentContainer){
        parentContainer.append(container);
      }

      return this;
    }

  },{
    ATTRS: {
      container: { valueFn: function(){return Y.one('#yui-table-result');} }
    }
  });

  Y.ApplicantListView = Y.Base.create('applicantListView', Y.View, [], {
    template: '<table id="yui-table-result"></table>',

    initializer: function () {
      var modelList = this.get('modelList');
      this.set('container',this.get('container').empty());
      this.after('modelList:add', this.renderApplicant, this);
      this.after('modelListChange', function (ev) {
        ev.prevVal && ev.prevVal.removeTarget(this);
        ev.prevVal && ev.newVal.addTarget(this);
      });

      modelList && modelList.addTarget(this);
    },

    _loadData: function(source){
      var modelList = this.get('modelList').reset();
      this._renderHeader(source.header);

      var content = source.getContent();
      for(var i = 0; i < content.length; i++){
        var applicant = new Y.Applicant(source.header);

        applicant.add(content[i]);
        modelList.add(applicant);
      }


    },

    render: function (source) {
      var container = this.get('container').empty();
      container.append(this.template);
      Y.one('body').append(container);

      this._loadData(source);
    },

    renderApplicant: function (ev) {
      var view = new Y.ApplicantView({model: ev.model});
      view.render(this.get('container'));
      return false;
    },

    _renderHeader: function(header){
      var applicant = new Y.Applicant(header);
      var view = new Y.ApplicantView({model: applicant});
      view.renderHeader(this.get('container'));
    }

  }, {
    ATTRS: {
      modelList: {value: new Y.ModelList({model: Y.Applicant })},
      container: {value: Y.Node.create('<div id="div-yui-result"></div>')}
    }
  });

  Y.augment(Y.Applicant, Y.Attribute);

}, '1.0', {requires: ['model', 'model-list', 'view','attribute']});


YUI().use('node', 'applicant', function(Y){
  var applicantListView = new Y.ApplicantListView();
  Y.one('#readableFileInput').on('change', function(){

    var files = document.getElementById('readableFileInput').files;
    Y.one('#div-yui-result').empty();
    var csvFile = new CSV();

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);
          applicantListView.render(csvFile);
        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }
  });
});