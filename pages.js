function Pages(){
  this.pages = [];
  this.totalPages = 0;
};

Pages.prototype = (function(){

  //prototype
  return{
    constructor:Pages,

    paginate: function(l, n){
      if (n < l){
        var total = Math.trunc(l/n)
        if(l%n > 0)
          total++
        for (var i=0; i < total + 1; i++) {
          this.pages.push(true);
        }
      }
      this.totalPages = total
    },

    repaginate: function(){
      this.pages = [];
      for (var i=0; i < this.totalPages + 1; i++) {
        this.pages.push(true);
      }
    },

    isPageLoaded: function(page){
      return this.pages[page] == true
    },

    max: function (page, n){
      return (page * n) + (n - 1)
    },

    min: function (page, n){
      if(page == 0) return 0
      return (page  * n )
    },

    markAsLoaded: function(page){
      this.pages[page] = true
    },

    markAsUnloaded: function(page){
      this.pages[page] = false
    },

    isFirst: function(page){
      return page == 0
    },

    isLast: function(page){
      return page == this.totalPages
    }
  };
})();


