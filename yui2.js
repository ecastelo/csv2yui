// widget configuration




YUI().use('node-scroll-info','datatable-mutable', function(Y){
  Y.one('#readableFileInput').on('change', function() {
    csvFile = new CSV();
    var files = document.getElementById('readableFileInput').files;

    if (files.length && csvFile.is_valid(files[0])) {
      csvFile.name = files[0];
      var reader = new FileReader();
      reader.onloadend = (function (e) {
        if (e.target.readyState == FileReader.DONE) {
          var content = e.target.result;
          csvFile.load(content);

          var config = {
            grid: {
              name: 'grid',
              show: {
                header 		: true,
                toolbar 	: true,
                footer		: true,
                lineNumbers	: true,
                selectColumn: true,
                expandColumn: true
              },
              columns: [
                { field: 'fname', caption: 'First Name', size: '30%' },
                { field: 'lname', caption: 'Last Name', size: '30%' },
                { field: 'email', caption: 'Email', size: '40%' },
                { field: 'sdate', caption: 'Start Date', size: '120px' },
              ],
              searches: [
                { type: 'int',  field: 'recid', caption: 'ID' },
                { type: 'text', field: 'fname', caption: 'First Name' },
                { type: 'text', field: 'lname', caption: 'Last Name' },
                { type: 'date', field: 'sdate', caption: 'Start Date' }
              ],
              url: './list.json'
            }
          }

          $('#main').w2grid(config.grid)

        }
      }).bind(csvFile);
      reader.readAsText(csvFile.name, 'UTF-8');
    }else{
      alert("It is not a valid CSV!");
    }









//
//    var config = {
//      grid: {
//        name: 'grid',
//        records : csvFile.lines,
//        show: {
//          footer	: true,
//          toolbar	: true
//        },
//        columns: csvFile.header
//      }
//    }

    function refreshGrid(auto) {
      w2ui.grid.skip(0);
    }

//    $(function () {
//      $('#main').w2grid(config.grid);
//    });

  });




});